import Vue from "vue";
import Router from "vue-router";
import Accueil from "@/components/Accueil";
import Menu from "@/components/Menu";
import Lobbies from "@/components/Lobbies";
import Lobby from "@/components/Lobby";
import Rules from "@/components/Rules";
import Options from "@/components/Options";
import Game from "@/components/Game";
import Score from "@/components/Score";
import Credits from "@/components/Credits";

Vue.use(Router);

const routes = [
  {
    path: "/",
    name: "Accueil",
    component: Accueil
  },
  {
    path: "/menu",
    name: "Menu",
    component: Menu
  },
  {
    path: "/rules",
    name: "Rules",
    component: Rules
  },
  {
    path: "/credits",
    name: "Credits",
    component: Credits
  },
  {
    path: "/options",
    name: "Options",
    component: Options
  },
  {
    path: "/lobbies",
    name: "Lobbies",
    component: Lobbies
  },
  {
    path: "/lobby/:id",
    name: "Lobby",
    component: Lobby
  },
  {
    path: "/game/:id",
    name: "Game",
    component: Game
  },
  {
    path: "/score/:id",
    name: "Score",
    component: Score
  }
];

const router = new Router({
  routes,
  mode: "history"
});

router.beforeEach((to, from, next) => {
  const user = window.localStorage.getItem("user");
  if (to.path !== "/") {
    if (!user) {
      next("/");
    } else {
      let data = JSON.parse(user);
      if (data.lobby && to.name !== "Lobby") {
        next(`/lobby/${data.lobby}`);
      } else if (data.game && to.name !== "Game") {
        next(`/game/${data.game}`);
      }
    }
  } else {
    if (user) {
      next("/menu");
    }
  }

  next();
});

export default router;
