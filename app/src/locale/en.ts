export default {
  button: {
    quit: "Quit",
    credits: "Credits",
    start: "Start",
    rules: "Rules",
    create: "Create",
    back: "Back",
    cancel: "Cancel",
    save: "Save"
  },
  home: {
    welcome: "Welcome Agent",
    username_placeholder: "Pick a username"
  },
  menu: {
    welcome: "Welcome Agent",
    play: "Play",
    options: "Options",
    logout: "Logout"
  },
  rules: {
    introduction: `CodeNames is a game of guessing
    which code names (words) in a set are related
    to a hint-word given by another playe.`,
    teams: `Players split into two teams: red and blue. One player of each team
    is selected as the team's spymaster. The others are field operatives. `,
    cards: `Twenty-five code name cards, each bearing a word, are laid out in
    a 5x5 rectangular grid, in random order.`,
    words: `A number of these words represent red agents, a number represent
    blue agents, one represents an assassin (selecting this card result in the
      immediate loss of the team that picked it),
    and the others represent innocent bystanders. `,
    more: `Find out more informations about CodeNames on`
  },
  options: {
    username: "Enter a new username",
    language: "Default language",
    sound: "Sound",
    volume: "Volume",
    miscs: "Miscallaneous"
  },
  lobbies: {
    title: "Lobbies room",
    agent: "Agent",
    table: {
      name: "Game name",
      id: "Identifier",
      slot: "Players",
      lock: "Lock",
      lang: "Language"
    },
    create_form: {
      title: "Create a lobby",
      name: "Name",
      password: "Password (optional)",
      language: "Language",
      difficulty: {
        title: "Difficulty",
        easy: "Easy",
        medium: "Medium",
        hard: "Hard",
        extreme: "To the moon"
      }
    }
  },
  lobby: {
    quit: "Leave",
    team_blue: "Blue Team",
    team_red: "Red Team",
    no_team: "Pick a team",
    rdy: "Ready",
    not_rdy: "Not Ready",
    send: "Send",
    type: "Type here"
  },
  game: {
    welcome: "Welcome",
    quit: "Leave the game",
    role: "You are the",
    playing: "Team's turn",
    clue: "The clue",
    associations: "Number of associations",
    send_clue: "Send clue",
    clue_from: "Clue from the agent",
    team_blue: "Blue Team",
    team_red: "Red Team",
    time_left: "Time left",
    options: {
      title: "Settings",
      sound: "Sound",
      volume: "Volume",
      save: "Save",
      back: "Back"
    }
  },
  score: {
      team: "Team",
      winer: "won the game",
      team_blue: "Blue Team",
      team_red: "Red Team",
      player_left: "left the game and thus his team lost.",
      black_card: "The black card was picked by losing team.",
      lobby_back: "Back to lobby"
  },
  server: {
    chat: {
      player_join_lobby: "new player in lobby"
    }
  }
};
