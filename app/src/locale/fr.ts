export default {
  button: {
    quit: "Quitter",
    credits: "Credits",
    start: "Commencer",
    rules: "Regles",
    create: "Créer",
    back: "Retour",
    cancel: "Annuler",
    save: "Sauvegarder"
  },
  home: {
    welcome: "Bienvenue Agent",
    username_placeholder: "Entrez votre nom d'utilisateur"
  },
  menu: {
    welcome: "Bonjour Agent",
    play: "Jouer",
    options: "Options",
    logout: "Deconnexion"
  },
  rules: {
    introduction: `CodeNames est un jeu de questions où les noms de code (mots)
     d'un ensemble sont reliés à un indice donné par un autre joueur.`,
    teams: `Les joueurs sont séparés en deux équipes : les rouges et les bleus.
    Un joueur de chaque équipe est désigné comme étant le maître-espion de son équipe.
    Les autres joueurs appelés les informateurs.`,
    cards: `25 cartes avec un nom de code sont placées sur une grille de 5x5,
    dans un ordre aléatoire.`,
    words: `Un certain nombre de ces cartes sont liées à l'équipe rouge, un autre
    nombre est lié à l'équipe bleue, et une carte représente un assassin
    (sélectionner cette carte entraîne la défaite immédiate de l'équipe étant tombée
    dessus). Le reste des cartes représente des spectateurs innocents (neutre).`,
    more: `Vous trouverez plus d'informations sur CodeNames sur`
  },
  options: {
    username: "Entrez un nom d'utilisateur",
    language: "Language par défaut",
    sound: "Son",
    volume: "Volume",
    miscs: "Divers (bruitages)"
  },
  lobbies: {
    title: "Salon des lobbies",
    agent: "Agent",
    table: {
      name: "Nom de la partie",
      id: "Identifiant",
      slot: "Joueurs",
      lock: "Verrouiller",
      lang: "Langue"
    },
    create_form: {
      title: "Créer un lobby",
      name: "Nom",
      password: "Mot de passe (facultatif)",
      language: "Langue",
      difficulty: {
        title: "Difficulté",
        easy: "Facile",
        medium: "Normal",
        hard: "difficile",
        extreme: "extrême"
      }
    }
  },
  lobby: {
    quit: "Quitter",
    team_blue: "Equipe Bleue",
    team_red: "Equipe Rouge",
    no_team: "Choisir un equipe",
    rdy: "Prêt",
    not_rdy: "Pas Prêt",
    type: "Ecrire ici",
    send: "Envoyer",
    no_team: "Choisissez une équipe"
  },
  game: {
    welcome: "Bienvenue",
    quit: "Quitter la partie",
    role: "Vous êtes le",
    playing: "C'est le tour de l'équipe",
    clue: "L'indice",
    associations: "Nombre d'associations",
    send_clue: "Envoyer indice",
    clue_from: "Indice de l'informateur",
    team_blue: "Equipe bleue",
    team_red: "Equipe rouge",
    time_left: "Temps restant",
    options: {
      title: "Options",
      sound: "Son",
      volume: "Volume",
      save: "Sauvegarder",
      back: "Retour"
    }
  },
  score: {
    team: "L'équipe",
    winer: "a gagné la partie",
    team_blue: "Equipe Bleue",
    team_red: "Equipe Rouge",
    lobby_back: "Retour au lobby",
    player_left: " a quitte le jeu, l'equipe adverse a donc gagne la partie",
    black_card: "La carte piege a ete tiree par l'equipe perdante."
  },
  server: {
    chat: {
      player_join_lobby: "Nouveau joueur dans le lobby"
    }
  }
};
