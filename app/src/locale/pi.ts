export default {
  server: {
    chat: {
      player_join_lobby: "Arg, a man joined the vessel"
    }
  },
  button: {
    quit: "Quit",
    credits: "Helpin' crew",
    start: "Get in th' ship sea man",
    rules: "Royal rules",
    create: "Make my ship",
    back: "Abort mission",
    cancel: "Argh, no",
    save: "Save"
  },
  home: {
    welcome: "Argh Arh Captain",
    username_placeholder: "Whats ya lil name"
  },
  menu: {
    welcome: "Argh argh hoi seadog",
    play: "Get in the ship",
    options: "Fix the ship",
    logout: "Leave the crew"
  },
  rules: {
    introduction: `CodeNames is a game of guessing
    which code names (words) in a set are related
    to a hint-word given by another playe.`,
    teams: `Players split into two teams: red and blue. One player of each team
    is selected as the team's spymaster. The others are field operatives. `,
    cards: `Twenty-five code name cards, each bearing a word, are laid out in
    a 5x5 rectangular grid, in random order.`,
    words: `A number of these words represent red agents, a number represent
    blue agents, one represents an assassin (selecting this card result in the
      immediate loss of the team that picked it),
    and the others represent innocent bystanders. `,
    more: `Find out more informations about CodeNames on`
  },
  options: {
    username: "Change ya name, seadog",
    language: "whats ya language",
    sound: "Sound",
    volume: "Shout power",
    miscs: "Treasure.."
  },
  lobbies: {
    title: "Royal Port",
    agent: "Agent",
    table: {
      name: "Vessel lil name",
      id: "County name",
      slot: "Crew size",
      lock: "Key",
      lang: "Dialect"
    },
    create_form: {
      title: "Create a lobby",
      name: "Name",
      password: "Password (optional)",
      language: "Language",
      difficulty: {
        title: "Difficulty",
        easy: "Easy",
        medium: "Medium",
        hard: "Hard",
        extreme: "To the moon"
      }
    }
  },
  lobby: {
    quit: "Back to port",
    team_blue: "Blue Team",
    team_red: "Red Team",
    no_team: "choose ya crew",
    rdy: "Arg! I'm ready",
    not_rdy: "Erh, not ready",
    send: "Shout",
    type: "Whats' you gott say pirate?",
    no_team: "Select a crew"
  },
  score: {
    team: "Argh.., it's th' team",
    winer: "that won the battle",
    team_blue: "Blue Team",
    team_red: "Red Team",
    lobby_back: "Back to lobby",
    player_left: ", screw hem, left th' crew",
    black_card: "We lost to black magic!!"
  },
  game: {
    welcome: "Welcome",
    quit: "Jump out the vessel",
    role: "You are the",
    playing: "Team's turn",
    clue: "The clue",
    associations: "Number of associations",
    send_clue: "Send clue",
    clue_from: "Clue from the agent",
    team_blue: "Blue Team",
    team_red: "Red Team",
    time_left: "Time left",
    options: {
      title: "Settings",
      sound: "Shout?",
      volume: "Shout force",
      save: "Save",
      back: "Back"
    }
  }
};
