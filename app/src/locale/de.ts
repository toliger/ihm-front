export default {
  button: {
    quit: "Spiel verlassen",
    credits: "Kredit",
    start: "Beginnen",
    rules: "Regeln",
    create: "Erstellen",
    back: "Zurück",
    cancel: "Abbrechen",
    save: "Speichern"
  },
  home: {
    welcome: "Wilkommen Agent",
    username_placeholder: "Bitte geben Sie ihren Username ein"
  },
  menu: {
    welcome: "Hallo Agent",
    play: "Spielen",
    options: "Optionen",
    logout: "Sich ausloggen"
  },
  rules: {
    introduction: `CodeNames ist ein Fragenspiel, in dennen man Code Names, also
    Wörter von einer Gruppe, mit ein Hinweis von den anderen Spieler verbindet.`,
    teams: `Die Spielern werden in zwei Teams verteilt : die Rote und die Blaue.
    Ein Spieler jeder Team ist als Spion-Master des Teams ernannt.
    Die andere Spieler werden Informanten genannt.`,
    cards: `25 Karte mit ein Code Name werden auf ein 5*5 Gitter im kompletten
    zufällige Reihenfolge platziert.`,
    words: `Manchen diese Karte gehören der Rote Team, manchen der Blaue, une
    eine Karte repräsentiert ein Mörder (wenn Ihr diese Karte auspicken verliert
    Ihre Team direkt). Der rest der Karten repräsentieren unschuldingen Zuschauern.`,
    more: `Sie werden mehr Information über CodeNames hier finden :`
  },
  options: {
    username: "Bitte geben Sie ihren Username ein",
    language: "Standardeinstellung Sprache",
    sound: "Ton",
    volume: "Lautstärke",
    miscs: "Verschiedene"
  },
  lobbies: {
    title: "Lobbies lounge",
    agent: "Agent",
    table: {
      name: "Spielname",
      id: "Login",
      slot: "Spielern",
      lock: "Sperren",
      lang: "Sprache"
    },
    create_form: {
      title: "Ein Lobby erstellen",
      name: "Name",
      password: "Passwort",
      language: "Sprache",
      difficulty: {
        title: "Schwierigkeit",
        easy: "Leicht",
        medium: "Normal",
        hard: "Schwierig",
        extreme: "Extrem"
      }
    }
  },
  lobby: {
    quit: "Spiel Verlassen",
    team_blue: "Team Blau",
    team_red: "Team Rot",
    no_team: "Wähle ein Team aus",
    rdy: "Bereit",
    not_rdy: "Nicht bereit",
    type: "Hier schreiben",
    send: "Senden",
  },
  game: {
    welcome: "Wilkommen",
    quit: "Spiel verlassen",
    role: "Ihr seit der",
    playing: "An der Reihe ist Team",
    clue: "Das Hinweis",
    associations: "Anzahl von Vereinigungen",
    send_clue: "Das Hinweis senden",
    clue_from: "Hinweis von der Informant",
    team_blue: "Team Blau",
    team_red: "Team Rot",
    time_left: "Übliche Zeit",
    options: {
      title: "Optionen",
      sound: "Ton",
      volume: "Lautstärke",
      save: "Speichern",
      back: "Rückkehren"
    }
  },
  score: {
    team: "Team",
    winer: "hat das Spiel gewonnen",
    team_blue: "Team Blau",
    team_red: "Team Rot",
    player_left: "hat das Spiel verlassen, deshald verlor sein Team.",
    black_card: "Die schwarze Spielkarte wurde von die Verlierer gezogen.",
    lobby_back: "Rückkehr zum Lobby"
  },
  server: {
    chat: {
      player_join_lobby: "Neuer Spieler in die Lobby"
    }
  }
};
