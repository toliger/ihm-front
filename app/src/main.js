// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import VueSocketio from "vue-socket.io";
import VModal from "vue-js-modal";
import VueChatScroll from "vue-chat-scroll";
import "vue-material-design-icons/styles.css";
import App from "./App";
import router from "./router";
import MenuIcon from "vue-material-design-icons/Menu.vue";
import SettingsIcon from "vue-material-design-icons/Settings.vue";
import CloseCircleIcon from "vue-material-design-icons/CloseCircle.vue";

// Locales
import Vuetify from "vuetify";
import en from "./locale/en.ts";
import fr from "./locale/fr.ts";
import pi from "./locale/pi.ts";
import de from "./locale/de.ts";

Vue.config.productionTip = false;

Vue.use(VueSocketio, process.env.API_URI);
Vue.use(VModal);
Vue.use(VueChatScroll);
Vue.component("menu-icon", MenuIcon);
Vue.component("settings-icon", SettingsIcon);
Vue.component("close-icon", CloseCircleIcon);

Vue.use(Vuetify, {
  lang: {
    locales: {
      en,
      fr,
      pi,
      de
    },
    current: "en"
  }
});

/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  components: {
    App
  },
  template: "<App/>"
});
